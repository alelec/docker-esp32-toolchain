FROM python:3
MAINTAINER https://gitlab.com/alelec/docker-esp-toolchains/

ARG ESPIDF_SUPHASH=310beae373446ceb9a4ad9b36b5428d7fdf2705f

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        git zip \
    && rm -rf /var/lib/apt/lists/*

RUN pip3 install pyserial pyparsing==2.3.1

RUN cd / && \
    git clone https://github.com/espressif/esp-idf.git esp && \
    cd esp && \
    git checkout $ESPIDF_SUPHASH && \
    git submodule update --init --recursive
    
RUN cd /esp && \
    wget https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz && \
    tar -xzf xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz && \
    rm xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz
    
ENV ESPIDF /esp
ENV PATH="/esp/xtensa-esp32-elf/bin:${PATH}"